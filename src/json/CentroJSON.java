package json;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import Negocio.CentroDistribucion;


public class CentroJSON {
	ArrayList<CentroDistribucion> centros = new ArrayList<CentroDistribucion>();

	public void agregarCentro(CentroDistribucion c) {
		centros.add(c);
	}
	public ArrayList<CentroDistribucion> dameCentros() {
		return centros;
	}
	
	public void generarJSON(String archivo) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();		
		try {
			FileWriter writer = new FileWriter(archivo);
			writer.write(gson.toJson(this));
			writer.close();
		} catch (Exception e) {

		}
	}
	
	public CentroJSON leerJSON(String archivo)
	{
		Gson gson =new Gson();
		CentroJSON ret =null;
		try
		{
			ret = gson.fromJson(new BufferedReader(new FileReader(archivo)), CentroJSON.class);
			}
		catch(Exception e){
			
		}
		return ret;
		}
}