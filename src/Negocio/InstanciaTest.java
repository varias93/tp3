package Negocio;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;



public class InstanciaTest {
	private Instancia instancia;
	private ArrayList<CentroDistribucion> centrosEjemplo;
	private ArrayList<Cliente> clientesEjemplo;
	private ArrayList<Cliente> clientes;

	@Before	
	public void inicializar()	{	
		instancia = new Instancia("centros.JSON", "clientes.JSON");
		centrosEjemplo=CentrosEjemplos();
		clientesEjemplo=ClientesEjemplos();
		clientes = instancia.getClientes();
	}
	
	@Test
	public void constructorCentrosTest() {			
		assertEquals(instancia.getCentros(), centrosEjemplo);
	}

	@Test
	public void constructorClientesTest() {	
		assertEquals(instancia.getClientes(), clientesEjemplo);
	}

	@Test
	public void calcularDistanciaPromedioTest() {		
		CentroDistribucion c = centrosEjemplo.get(0);
		instancia.calcularDistanciaPromedio(c);
		assertEquals(c.getDistanciaPromedio(),4560.285714285715 , 0.02);
	}

	@Test
	public void calcularDistanciaTest() {	
		CentroDistribucion c = new CentroDistribucion(-34.532804,-58.708631);
		Cliente l = new Cliente(-34.541422, -58.697573);
		int distancia = instancia.calcularDistancia(l, c);			
		assertEquals(distancia, 1394);
	}

	@Test
	public void centroMasCercanoTest() {		
		Cliente l = clientes.get(0);
		int distancia = instancia.centroMasCercano(l,instancia.getCentros());
		CentroDistribucion c1= new CentroDistribucion(-34.532804,-58.708631);
		CentroDistribucion c2=instancia.getCentroPorCliente().get(l);		
		assertEquals(distancia, 632);
		assertEquals(c1,c2);
		
	}

	@Test
	public void solucionTest() {
		ArrayList<CentroDistribucion> centros = instancia.solucion(2);
		CentroDistribucion c1 = new CentroDistribucion(-34.532804,-58.708631);
		CentroDistribucion c2 = new CentroDistribucion(-34.53273, -58.725402);
		ArrayList<CentroDistribucion> centrosEj = new ArrayList<CentroDistribucion>();
		centrosEj.add(c1);
		centrosEj.add(c2);
		assertEquals(centros, centrosEj);

	}

	@Test
	public void calcularCostoTest() {	
		assertEquals(instancia.calcularCostoTotal(), 2147483641);

	}
	
	@Test
	public void calcularCostoConSolucionTest() {	
		instancia.solucion(2);	
		assertEquals(instancia.calcularCostoTotal(), 10161);

	}

	private ArrayList<Cliente> ClientesEjemplos() {
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		Cliente l1 = new Cliente(-34.527119, -58.708664);
		Cliente l2 = new Cliente(-34.541422, -58.697573);
		Cliente l3 = new Cliente(-34.523366, -58.693096);
		Cliente l4 = new Cliente(-34.531009, -58.73458);
		Cliente l5 = new Cliente(-34.539318, -58.714083);
		Cliente l6 = new Cliente(-34.546182, -58.724635);
		Cliente l7 = new Cliente(-34.504746, -58.711325);
		clientes.add(l1);
		clientes.add(l2);
		clientes.add(l3);
		clientes.add(l4);
		clientes.add(l5);
		clientes.add(l6);
		clientes.add(l7);
		return clientes;
	}

	private ArrayList<CentroDistribucion> CentrosEjemplos() {
		ArrayList<CentroDistribucion> centros = new ArrayList<CentroDistribucion>();
		CentroDistribucion c1 = new CentroDistribucion(-34.505213, -58.675616);
		CentroDistribucion c2 = new CentroDistribucion(-34.532804, -58.708631);
		CentroDistribucion c3 = new CentroDistribucion(-34.53273, -58.725402);
		CentroDistribucion c4 = new CentroDistribucion(-34.504941, -58.720703);
		centros.add(c1);
		centros.add(c2);
		centros.add(c3);
		centros.add(c4);
		return centros;
	}

}