package Negocio;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import json.CentroJSON;
import json.ClienteJSON;

public class Instancia {
	ArrayList<CentroDistribucion> centros;
	ArrayList<Cliente> clientes;
	int _cantidadDeCentros;
	CentroJSON archivoCentros=new CentroJSON();
	ClienteJSON archivoClientes= new ClienteJSON();
	HashMap<Cliente,CentroDistribucion> centroPorCliente= new HashMap<Cliente,CentroDistribucion>();
	ArrayList<CentroDistribucion> ret = new ArrayList<CentroDistribucion>();	
	URL direccion;
	
	public Instancia( String centros, String clientes) {
		super();
		direccion=getClass().getResource("../Archivos/"); 		
		archivoCentros= archivoCentros.leerJSON(direccion.getPath()+centros);
		this.centros = archivoCentros.dameCentros();
		archivoClientes=archivoClientes.leerJSON(direccion.getPath()+clientes);
		this.clientes = archivoClientes.dameClientes();
	}
	
	public ArrayList<CentroDistribucion> solucion(int cantidadDeCentros){
		_cantidadDeCentros=cantidadDeCentros;			
		for(CentroDistribucion c:centros) {
			calcularDistanciaPromedio(c);
			}
		OrdenarCentros(centros);
		
		for(int i=0; i<cantidadDeCentros;i++) {
			ret.add(centros.get(i));
		}
		
		return ret;
		
	}
	private void OrdenarCentros(ArrayList<CentroDistribucion> ret) {
		Collections.sort(ret,new Comparator<CentroDistribucion>() {	
			@Override
	        public int compare(CentroDistribucion uno, CentroDistribucion otro){
	             if( uno.getDistanciaPromedio() < otro.getDistanciaPromedio() ) 
	                 return -1;
	             else if( uno.getDistanciaPromedio() == otro.getDistanciaPromedio() ) 
	                 return 0;
	             else 
	                 return 1;
	         }			
		});
	}
	
	public void calcularDistanciaPromedio(CentroDistribucion centro) {
		double sumaDistancia=0;
		for (Cliente cliente: clientes) {
			sumaDistancia+= calcularDistancia(cliente,centro);
		}
		centro.setDistanciaPromedio(sumaDistancia/(double) clientes.size());
		
	}
	
	public int calcularCostoTotal() {
		int costoTotal=0;
		for (Cliente cliente: clientes) {
			costoTotal+=centroMasCercano(cliente, ret);
		}
		return costoTotal;
	}
	
	public int centroMasCercano(Cliente c, ArrayList<CentroDistribucion> ret) {
		int min=Integer.MAX_VALUE;
		CentroDistribucion nuevo=null;
		for (CentroDistribucion centro: ret) {
			int distancia=calcularDistancia(c,centro);
			if(distancia<min) {
				min=distancia;
				nuevo=centro;					
			}
		}
		centroPorCliente.put(c, nuevo);
		return min;
	}
	
	public void guardarSolucion() {
		try {
		 FileOutputStream fos = new FileOutputStream("Solucion.txt");
		OutputStreamWriter out = new OutputStreamWriter(fos);
		out.write("Los centros a abrir son: "+ret+"\nEl costo total es de: "+calcularCostoTotal());		
		out.close();
		}
		catch(Exception e) { }		
	}
	
//CALCULA LA DISTANCIA EN METROS
	int calcularDistancia(Ubicacion cliente,Ubicacion centro) 
	{
		double radioterrestre = 6371; 
		double latCliente = Math.toRadians(cliente.get_latitud());
		double lonCliente = Math.toRadians(cliente.get_longitud());
		double latCentro = Math.toRadians(centro.get_latitud());
		double lonCentro = Math.toRadians(centro.get_longitud());		

		double dlon = (lonCentro - lonCliente);
		double dlat = (latCentro - latCliente);

		double senolat = Math.sin(dlat / 2);
		double senolon = Math.sin(dlon / 2);

		double a = (senolat * senolat) + Math.cos(latCliente) * Math.cos(latCentro) * (senolon * senolon);
		double c = 2 * Math.asin(Math.min(1.0, Math.sqrt(a)));

		double distancia = radioterrestre * c * 1000;

		return (int) distancia;

	}		
	public ArrayList<CentroDistribucion> getCentros() {
		return centros;
	}
	public ArrayList<Cliente> getClientes() {
		return clientes;
	}
	public HashMap<Cliente, CentroDistribucion> getCentroPorCliente() {
		return centroPorCliente;
	}

}
